

from gnuradio import gr, blocks, analog
from gnuradio import uhd
from PyQt4 import Qt
from gnuradio import qtgui
import sys, sip
from msg_rx import msg_rx

class msg_tx_test(gr.top_block):
    def __init__(self):
        gr.top_block.__init__(self)

        sampRate = 2000000
        bitRate = 400000
        freq = 915e6

        # USRP
        usrp_rx = uhd.single_usrp_source('addr=192.168.10.2', uhd.io_type_t.COMPLEX_FLOAT32, 1)
        usrp_rx.set_samp_rate(sampRate)
        usrp_rx.set_bandwidth(1000)
        usrp_rx.set_subdev_spec("A:0")
        usrp_rx.set_center_freq(freq, 0)
        usrp_rx.set_antenna('TX/RX',0)
        usrp_rx.set_gain(20)

        # Instrumentation
        self.time_sink = qtgui.time_sink_c(60 * int(sampRate / bitRate), sampRate, "Receive test")
        self.snk_win = sip.wrapinstance(self.time_sink.pyqwidget(), Qt.QWidget)
        self.snk_win.show()

        rx = msg_rx(sampRate, bitRate, receive_callback)
        self.connect(usrp_rx, rx)
        self.connect(usrp_rx, self.time_sink)


def receive_callback():
    print "Received"

def main():
    qapp = Qt.QApplication(sys.argv)

    top = msg_tx_test()
    top.start()

    qapp.exec_()
    top.stop()

if __name__ == "__main__":
    main()
