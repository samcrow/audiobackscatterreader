

from gnuradio import gr, blocks, analog
from gnuradio import uhd
from PyQt4 import Qt
from gnuradio import qtgui
import sys, sip
from msg_rx import msg_rx

class msg_tx_test(gr.top_block):
    def __init__(self):
        gr.top_block.__init__(self)

        sampRate = 2000000
        bitRate = 400000
        freq = 915e6

        in_file = blocks.file_source(gr.sizeof_gr_complex, '../../../Capture/2016-05-27-1 data')
        throttle = blocks.throttle(gr.sizeof_gr_complex, sampRate / 100)
        # Multiply to make values large enough
        multiply = blocks.multiply_const_cc(300)

        # Instrumentation
        self.time_sink = qtgui.time_sink_c(60 * int(sampRate / bitRate), sampRate, "Receive test")
        self.snk_win = sip.wrapinstance(self.time_sink.pyqwidget(), Qt.QWidget)
        self.snk_win.show()

        self.digital_sink = qtgui.time_sink_f(60 * int(sampRate / bitRate), sampRate, "Thresholded")
        self.digital_win = sip.wrapinstance(self.digital_sink.pyqwidget(), Qt.QWidget)
        self.digital_win.show()


        rx = msg_rx(sampRate, bitRate, receive_callback)
        self.connect(in_file, multiply)
        self.connect(multiply, throttle)
        self.connect(throttle, rx)
        self.connect(throttle, self.time_sink)

        # Output
        self.connect(rx, self.digital_sink)

        out_file = blocks.file_sink(gr.sizeof_float, '../../../Capture/2016-05-27-1 digital')
        self.connect(rx, out_file)


def receive_callback():
    print "Received"

def main():
    qapp = Qt.QApplication(sys.argv)

    top = msg_tx_test()
    top.start()

    qapp.exec_()
    top.stop()

if __name__ == "__main__":
    main()
