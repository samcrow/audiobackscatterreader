
"""
Holds utils for building and decoding
messages in the ambient backscatter format.
"""

def frame(tagID, payload):
    preamble0 = 0
    preamble1 = 3

    msgLen = len(payload)
    checksum = abc.calc_checksum(tagID, msgLen, 1, payload)

    msg = struct.pack("BBBBB" + str(msgLen) + "sB", preamble0, preamble1, tagID, msgLen, 1, payload, checksum)

    #print(len(payload))

    #print(':'.join(x.encode('hex') for x in msg))

    return msg

def deframe(header, raw_payload):
    tagID, msgLen = struct.unpack("BB", bitarray(header).tobytes())
    #print("---------------------------------------")
    #print("TagID: " + str(tagID))
    #print("MsgLen: " + str(msgLen))

    payload, tag_checksum = struct.unpack(str(msgLen) + "s" + "B", bitarray(raw_payload).tobytes())
    local_checksum = abc.calc_checksum(tagID, msgLen, 1, payload)

    #print("Payload: " + str(ord(payload[0])))
    #print(bitarray(raw_payload))
    #print("CRC: " + str(tag_checksum))
    #print("Local CRC: " + str(local_checksum))

    if local_checksum != tag_checksum:
        return (None, None)

    return (tagID, payload)

def getPreamble():
    return (0,0,0,0,0,0,1,1)

def getHeaderLen():
    return 2*8

def getPayloadLen(header):
    return 8*struct.unpack_from("B", bitarray(header[8:16]).tobytes())[0] + 8

def calc_checksum(tagID, msgLen, msgID, payload):
    payload_sum = sum(struct.unpack("B"*msgLen, payload))
    header_sum = tagID + msgLen + msgID
    return (payload_sum+header_sum)%256
