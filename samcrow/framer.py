import numpy
import collections
from gnuradio import gr
import backscatter_frame


class framer(gr.sync_block):
    """
    """
    def __init__(self, tag_callback):
        gr.sync_block.__init__(self,
            name="backscatter_framer",
            in_sig=[numpy.int8],
            out_sig=[])

        self.tag_callback = tag_callback

        self.preamble_ref = collections.deque(backscatter_frame.getPreamble())
        self.preamble = collections.deque([], len(backscatter_frame.getPreamble()))
        self.header = collections.deque([])
        self.payload = collections.deque([])


    def work(self, input_items, output_items):
        in0 = input_items[0]

        for bit in in0:
            if self.preamble == self.preamble_ref:
                if len(self.header) == backscatter_frame.getHeaderLen():
                    if len(self.payload) == backscatter_frame.getPayloadLen(list(self.header)):
                        self.tag_callback(*backscatter_frame.deframe(list(self.header), list(self.payload)))
                        self.payload.clear()
                        self.header.clear()
                        self.preamble.clear()
                    else:
                        self.payload.append(bit)
                else:
                    self.header.append(bit)
            else:
                self.preamble.append(bit)
                print self.preamble

        if len(self.header) == backscatter_frame.getHeaderLen() and len(self.payload) == backscatter_frame.getPayloadLen(list(self.header)):
            self.tag_callback(*backscatter_frame.deframe(list(self.header), list(self.payload)))
            self.payload.clear()
            self.header.clear()
            self.preamble.clear()

        return len(in0)
