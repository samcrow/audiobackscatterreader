
import numpy
from gnuradio import gr
from gnuradio import blocks
from gnuradio import analog
from fm0_decode import fm0_decode
from framer import framer

class msg_rx(gr.hier_block2):
    def __init__(self, sampRate, bitRate, tag_callback):
        gr.hier_block2.__init__(self, "msg_rx",
                                gr.io_signature(1, 1, gr.sizeof_gr_complex),
                                gr.io_signature(1, 1, gr.sizeof_float))

        # TODO: add skip head block to fix startup issue.

        squelch = analog.simple_squelch_cc(-10, 1)
        agc = analog.agc2_cc(attack_rate=1.6, decay_rate=5e-5, reference=1, gain=1.0)
        # agc.set_max_gain(1000)
        thresh_middle=0.5
        thresh = blocks.threshold_ff(thresh_middle-0.02,thresh_middle+0.02)
        fm0decode = fm0_decode(sampRate, bitRate)
        self.framer = framer(tag_callback)

        self.connect(self, squelch, agc, blocks.complex_to_mag_squared(), thresh, blocks.float_to_char(), fm0decode, self.framer)
        self.connect(thresh, self)
