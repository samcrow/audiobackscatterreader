#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import numpy
import itertools
from gnuradio import gr

class fm0_encode(gr.interp_block):
    """
    An FM0 encoder

    Takes a data signal of int8s, containing 0 and 1 values

    Outputs an FM0-encoded stream of 0 and 1 values
    """
    def __init__(self):
        # The last output value, 0 or 1
        self.previous = 1

        gr.interp_block.__init__(self,
            name="fm0_encode",
            in_sig=[numpy.int8],
            # Interpolation of 2: 2 output samples for each input
            out_sig=[numpy.int8], interp = 2)

    def work(self, input_items, output_items):
        signal = input_items[0]
        out = output_items[0]

        for i, signalbit in enumerate(signal):
            if signalbit:
                # One sample the opposite of previous, then one previous
                out[2 * i] = invert(self.previous);
                out[(2 * i) + 1] = self.previous;
                # self.previous does not change
            else:
                # Two samples opposite of previous
                inverted = invert(self.previous)
                out[2 * i] = inverted
                out[(2 * i) + 1] = inverted
                self.previous = inverted
        return len(out)

# Inverts a bit. Converts 1 to 0 and 0 to 1.
def invert(b):
    return int(not b)
