

from msg_tx import msg_tx
from gnuradio import gr, blocks, analog
from gnuradio import uhd
from PyQt4 import Qt
from gnuradio import qtgui
import sys, sip
import time
import threading

class msg_tx_test(gr.top_block):
    def __init__(self):
        gr.top_block.__init__(self)

        sampRate = 2000000
        bitRate = 328
        freq = 915e6

        # USRP
        # usrp_tx = uhd.single_usrp_sink('addr=192.168.10.2', uhd.io_type_t.COMPLEX_FLOAT32,1)
        # usrp_tx.set_samp_rate(sampRate)
        # usrp_tx.set_subdev_spec("A:0") 
        # usrp_tx.set_center_freq(freq, 0)
        # usrp_tx.set_antenna('TX/RX',0)
        # usrp_tx.set_gain(10)

        # constant = analog.sig_source_c(sampRate, analog.GR_CONST_WAVE,
        #                         1, 1)

        # file = blocks.file_sink(gr.sizeof_char, "write_data")

        self.mtx = msg_tx(sampRate, bitRate)
        self.to_float = blocks.char_to_float(1)
        self.to_complex = blocks.float_to_complex()
        self.connect(self.mtx, self.to_float)
        self.connect(self.to_float, self.to_complex)
        # self.connect(self.to_complex, usrp_tx)
        # self.connect(constant, usrp_tx)

        # Instrumentation
        self.time_sink = qtgui.time_sink_c(60 * int(sampRate / bitRate), sampRate, "Send test")
        self.snk_win = sip.wrapinstance(self.time_sink.pyqwidget(), Qt.QWidget)
        self.snk_win.show()

        self.connect(self.to_complex, self.time_sink)
        # self.connect(constant, self.time_sink)
        # self.connect(self.mtx, file)

    def send(self, tag, payload):
        self.mtx.send(tag, payload)

def main():
    qapp = Qt.QApplication(sys.argv)

    top = msg_tx_test()
    top.start()

    thread_running = True
    def run():
        while thread_running:
            print "Sending"
            top.send(0, "\x00")
            print "Sent"
            time.sleep(1)

    thread = threading.Thread(group = None, target = run)
    thread.start()

    qapp.exec_()

    top.stop()
    thread_running = False

if __name__ == "__main__":
    main()
