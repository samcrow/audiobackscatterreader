#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import numpy
import struct
from gnuradio import gr
from gnuradio import blocks
from pie_encode import pie_encode
from pref_multiplex import pref_multiplex
import abc


class msg_tx(gr.hier_block2):
    """
    Encodes and outputs messages

    Outputs a sequence of 0/1 values suitable for sending at the provided
    sample rate.
    """
    def __init__(self, sample_rate, bit_rate):
        gr.hier_block2.__init__(self,
            name = "msg_tx",
            input_signature = gr.io_signature(0, 0, 0),
            output_signature = gr.io_signature(1, 1, gr.sizeof_char))

        # Calculate samples per bit
        # Divided by 2 because 2 samples are used for each bit
        # (there can actually be 3 for a zero bit, but that is ignored here.)
        samples_per_half_bit = int(sample_rate / bit_rate) / 2

        # PIE encoder
        pie = pie_encode()
        # Message source
        self.msg_queue = gr.msg_queue()
        msg = blocks.message_source(gr.sizeof_char, self.msg_queue)
        # Unpacker (separates bytes into bits)
        unpack = blocks.unpack_k_bits_bb(8)
        # Repeater, for achieving the correct bit rate
        repeat = blocks.repeat(gr.sizeof_char, samples_per_half_bit)
        # Multiplexer, to send 1s when not sending data
        multiplex = pref_multiplex()

        # Connections
        self.connect(msg, unpack)
        # Unpacked bits as signal input
        self.connect(unpack, pie)
        self.connect(pie, repeat)
        self.connect(repeat, multiplex)
        self.connect(multiplex, self)

    """
    Sends a message. The message should be a list of bytes.
    tag is the ID of the tag to send to.

    The message must be a string. If it is not, the program may freeze.
    """
    def send(self, tag, message):
        encoded = abc.frame(tag, message)
        self.msg_queue.insert_tail(gr.message_from_string(encoded))
