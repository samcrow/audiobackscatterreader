import numpy
from gnuradio import gr

class fm0_decode(gr.basic_block):
    """
    """
    def __init__(self, sampRate, bitRate):
        gr.basic_block.__init__(self,
            name="backscatter_fm0_decode",
            in_sig=[numpy.int8],
            out_sig=[numpy.int8])


        self.sampPerChip = int((sampRate / bitRate) / 2)

        self.prevSamp = 0
        self.numSamp = 0
        self.halfbitprev = 0

        self.numbits = 0


    def general_work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        halfbit = self.sampPerChip
        fullbit = 2 * halfbit
        wiggle = 5

        i = 0
        for samp in in0:
            if samp != self.prevSamp:
                # Full Bit
                if (fullbit - wiggle) <= self.numSamp <= (fullbit + wiggle):
                    self.halfbitprev = 0
                    out[i] = 1
                    i += 1

                # Half Bit
                elif (halfbit - wiggle) <= self.numSamp <= (halfbit + wiggle):
                    if self.halfbitprev:
                        self.halfbitprev = 0
                        out[i] = 0
                        i += 1
                    else:
                        self.halfbitprev = 1
                self.numSamp = 0

            self.numSamp += 1
            self.prevSamp = samp

        self.consume_each(len(in0))
        return i
